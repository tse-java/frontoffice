importScripts("https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js");

const config = {
  messagingSenderId: "343014077744",
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
