import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Login from "../views/Login.vue";
import SignUp from "../views/SignUp.vue";
import WebChat from "../views/Webchat.vue";
import Dashboard from "../views/Dashboard.vue";
import Patients from "../views/Patients.vue";
import Resources from "../views/Resources.vue";
import Exams from "../views/Exams.vue";
import store from "../store/index";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
    beforeEnter: (to, from, next) => {
      if (store.state.allowSignUp) {
        next();
      } else {
        next("/");
      }
    },
  },
  {
    path: "/webchat",
    name: "Webchat",
    component: WebChat,
    beforeEnter: (to, from, next) => {
      if (store.state.profile.isDoctor && store.state.logged) {
        next();
      } else {
        next("/");
      }
    },
  },
  {
    path: "/resources",
    name: "Resources",
    component: Resources,
  },
  {
    path: "/patients",
    name: "Patients",
    component: Patients,
    beforeEnter: (to, from, next) => {
      if (store.state.profile.isDoctor && store.state.logged) {
        next();
      } else {
        next("/");
      }
    },
  },
  {
    path: "/exams",
    name: "Exams",
    component: Exams,
    beforeEnter: (to, from, next) => {
      if (store.state.logged) {
        next();
      } else {
        next("/");
      }
    },
  },
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
