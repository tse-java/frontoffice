import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueAxios from "vue-axios";
// @ts-ignore
import VueAuthenticate from "vue-authenticate";
import notification from "vue-notification";
import axios from "axios";
import VueI18n from "vue-i18n";
import Locale from "./locale/index";

Vue.use(VueI18n);
Vue.use(VueAxios, axios);
Vue.use(notification);
Vue.use(VueAuthenticate, {
  providers: {
    gubUy: {
      clientId: "884442857108-vg56n3d25i4pibo750c7e9qjn6vu8i3f.apps.googleusercontent.com",
      redirectUri: window.location.origin,
      scope: ["profile", "email"],
      scopePrefix: "openid",
      scopeDelimiter: " ",
      requiredUrlParams: ["scope", "display"],
      oauthType: "2.0",
      display: "popup",
      popupOptions: { width: 452, height: 633 },
      responseType: "token",
      authorizationEndpoint: "https://accounts.google.com/o/oauth2/auth",
    },
  },
});

const i18n = new VueI18n({
  locale: store.state.lang,
  messages: { sp: Locale.sp, en: Locale.en },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");

export default { i18n };
