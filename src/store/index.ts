import Vue from "vue";
import Vuex from "vuex";
import VuePersist from "vuex-persist";
import i18n from "../main";

Vue.use(Vuex);

const persist = new VuePersist({
  reducer: (state: any) => ({
    lang: state.lang,
    token: state.token,
    logged: state.logged,
    refreshToken: state.refreshToken,
    profile: state.profile,
  }),
});

export default new Vuex.Store({
  state: {
    lang: "sp",
    profile: {
      isDoctor: null,
    },
    showNav: true,
    extended: false,
    token: "",
    logged: false,
    refreshToken: "",
    allowSignUp: false,
    menu: false,
  },
  mutations: {
    SET_TOKEN(state, value) {
      state.token = value.accessToken;
      state.refreshToken = value.refreshToken;
    },
    SET_SIGN_UP(state, value) {
      state.allowSignUp = value;
    },
    SET_LOGGED(state, value) {
      state.logged = value;
    },
    SET_EXTENDED(state, value) {
      state.extended = value;
    },
    SET_NAV(state, value) {
      state.showNav = value;
    },
    SET_PROFILE(state, value) {
      state.profile = value;
    },
    SET_LANG(state, value) {
      state.lang = value;
      i18n.i18n.locale = value;
    },
    SET_MENU(state, value) {
      state.menu = value;
    },
  },
  actions: {
    authenticate({ commit }, value) {
      commit("SET_TOKEN", value);
    },
    signUp({ commit }, value) {
      commit("SET_SIGN_UP", value);
    },
    login({ commit }, value) {
      commit("SET_LOGGED", value);
    },
    expandToolbar({ commit }, value) {
      commit("SET_EXTENDED", value);
    },
    showNav({ commit }, value) {
      commit("SET_NAV", value);
    },
    setProfile({ commit }, value) {
      commit("SET_PROFILE", value);
    },
    setLang({ commit }, value) {
      commit("SET_LANG", value);
    },
    setMenu({ commit }, value) {
      commit("SET_MENU", value);
    },
  },
  modules: {},
  getters: {
    token: (state) => {
      return state.token;
    },
  },
  plugins: [persist.plugin],
});
