import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface DiseaseType {
  name: string;
  id: number;
}

export class Disease {
  static async getDiseases(): Promise<Array<DiseaseType>> {
    try {
      const response = await axios.get("diseases");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
