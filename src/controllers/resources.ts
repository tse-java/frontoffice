import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface ResourceType {
  availability: Array<Availability>;
  dtoResource: {
    description: string;
    id: number;
    name: string;
    resourceType: string;
  };
  subscribed: boolean;
}

export interface Availability {
  quantity: number;
  resourceProvider: {
    address: {
      city: string;
      department: string;
      street: string;
      town: string;
    };
    commercialName: string;
    id: number;
    name: string;
    openSchedule: [
      {
        closingHour: string;
        dayOfWeek: string;
        openingHour: string;
      }
    ];
  };
}

export class Resource {
  static async getResources(): Promise<Array<ResourceType>> {
    try {
      const response = await axios.get("resources");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async getResourcesWithSuscriptions(): Promise<Array<ResourceType>> {
    try {
      const response = await axios.get("resources/subscriptions");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async subscribe(id: number): Promise<void> {
    try {
      await axios.post("resources/" + id + "/subscription");
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async unsubscribe(id: number): Promise<void> {
    try {
      await axios.delete("resources/" + id + "/subscription");
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
