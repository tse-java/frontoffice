import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface AuthResponse {
  accessToken: string;
  refreshToken: string;
  finishedUserProfile: boolean;
}

export class Auth {
  static async authenticate(token: { token: string }): Promise<AuthResponse> {
    try {
      const response = await axios.post("users/login", token);
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async subscribeToTopic(token: { token: any }): Promise<AuthResponse> {
    try {
      const response = await axios.post("firebaseSubscriptions", token);
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async logOut(): Promise<void> {
    try {
      await axios.post("users/logout");
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
