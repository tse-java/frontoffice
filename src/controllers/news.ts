import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface TwitterNewType {
  date: string;
  favoriteCount: number;
  mediaEntities: Array<{ mediaUrl: string }>;
  nickName: string;
  profilePhoto: string;
  retweetCount: number;
  text: string;
  type: string;
  userName: string;
  link: string;
}

export interface RssNewType {
  date: string;
  description: string;
  link: string;
  source: string;
  title: string;
  type: string;
}

export class New {
  static async getNews(): Promise<Array<RssNewType | TwitterNewType>> {
    try {
      const response = await axios.get("news");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
