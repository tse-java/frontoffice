import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface AppoinmentType {
  location: {
    city: string;
    street: string;
    department: string;
  };
  patient: {
    id: number;
    firstname: string;
    lastname: string;
  };
  doctor: {
    id: number;
  };
  firsnameAlias: string;
  lastnameAlias: string;
}

export class Appoinment {
  static async getAppointments(): Promise<Array<AppoinmentType>> {
    try {
      const response = await axios.get("appointments", { params: { requestAs: "medic" } });
      return response.data.map((appoinment: AppoinmentType) => {
        return Object.assign(appoinment, {
          firstnameAlias: appoinment.patient.firstname[0],
          lastnameAlias: appoinment.patient.lastname[0],
        });
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async closeAppointment(appoinmentId: number): Promise<void> {
    try {
      await axios.patch("appointments/" + appoinmentId);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
