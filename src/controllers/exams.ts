import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface ExamType {
  date: string;
  positive: boolean;
  status: string;
  medicalCase: {
    disease: {
      name: string;
    };
  };
  id: number;
}

export class Exam {
  static async getExams(userId: number): Promise<Array<ExamType>> {
    try {
      const response = await axios.get("exams/" + userId);
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
