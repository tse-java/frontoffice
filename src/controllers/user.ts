import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface SignUpData {
  documentNumber: string;
  phoneNumber: string;
  isDoctor: boolean;
  address: {
    coordinates: string;
    street: string;
    town: string;
    city: string;
    department: string;
  };
}

export interface ProfileData {
  address: {
    city: string;
    coordinates: string;
    department: string;
    street: string;
    town: string;
  };
  birthday: string;
  documentNumber: string;
  email: string;
  externalId: string;
  firstname: string;
  id: number;
  isDoctor: boolean;
  isFinished: boolean;
  lastname: string;
  nationality: string;
  phoneNumber: string;
}

export class User {
  static async signUp(data: SignUpData): Promise<void> {
    try {
      await axios.put("users", data);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async getProfile(): Promise<ProfileData> {
    try {
      const response = await axios.get("users/profile");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async requestExam(diseaseId: number, userId: number) {
    try {
      const response = await axios.post(`users/` + userId + `/exams`, { id: diseaseId });
      console.log(response);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async updateNotifiaction(push: boolean, mail: boolean): Promise<void> {
    try {
      await axios.patch("users/notification", { pushNotify: push, mailNotify: mail });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
