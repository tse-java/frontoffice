import { Axios } from "../axios";

const axios = new Axios().getInstance();

export interface CaseType {
  cases: Array<{ type: string; quantity: number }>;
  id: number;
  name: string;
}

export interface DepartmentType {
  id: number;
  name: string;
  cases: Array<CaseType>;
}

export class State {
  static async get(): Promise<Array<string>> {
    try {
      const response = await axios.get("/departments");
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  static async getCases(diseaseId: number): Promise<Array<CaseType>> {
    try {
      const response = await axios.get("/departments/cases", { params: { diseaseId: diseaseId } });
      return response.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
