import * as firebase from "firebase/app";
import "firebase/messaging";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyB2rux1EaETcXbLOj9c8RJgcOlrwQTAIIY",
  authDomain: "viruscontorl.firebaseapp.com",
  databaseURL: "https://viruscontorl.firebaseio.com",
  projectId: "viruscontorl",
  storageBucket: "viruscontorl.appspot.com",
  messagingSenderId: "343014077744",
  appId: "1:343014077744:web:1ab0a0d25b5da36f9b04f5",
  measurementId: "G-JZVK45H4EX",
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();
export const messaging = firebase.messaging();
