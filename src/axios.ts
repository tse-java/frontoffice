import axios, { AxiosInstance } from "axios";
import store from "./store/index";

export class Axios {
  private static axiosInstance: AxiosInstance;

  public getInstance(): AxiosInstance {
    if (!Axios.axiosInstance) {
      Axios.axiosInstance = axios.create({
        baseURL: process.env.VUE_APP_AXIOS_URI ? process.env.VUE_APP_AXIOS_URI : "/api",
        headers: { "content-type": "application/json", Authorization: `Bearer ${store.getters.token}` },
      });
      Axios.axiosInstance.interceptors.response.use(
        (response) => {
          return response;
        },
        (error) => {
          if (error.response.status !== 401 || error.response.config.url === "/users/refreshToken") {
            return new Promise((resolve, reject) => {
              reject();
            });
          }

          return Axios.axiosInstance
            .post("/users/refreshToken", { token: store.state.refreshToken })
            .then((data: any) => {
              const config = error.config;
              config.headers["Authorization"] = `Bearer ${data.data.accessToken}`;
              Axios.axiosInstance.defaults.headers["Authorization"] = `Bearer ${data.data.accessToken}`;

              return new Promise((resolve, reject) => {
                Axios.axiosInstance
                  .request(config)
                  .then((response) => {
                    store.dispatch("authenticate", {
                      accessToken: data.data.accessToken,
                      refreshToken: data.data.refreshToken,
                    });
                    resolve(response);
                  })
                  .catch((error) => {
                    reject(error);
                  });
              });
            })
            .catch((error) => {
              window.location.href = "/";
              store.dispatch("login", false);
              Promise.reject(error);
            });
        }
      );
    }
    return Axios.axiosInstance;
  }
}
